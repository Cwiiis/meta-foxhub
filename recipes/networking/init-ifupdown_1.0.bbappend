FILESEXTRAPATHS_prepend := "${THISDIR}/init-ifupdown:"

SRC_URI += " \
    file://iptables \
"

do_install_append() {
    install -m 0644 ${WORKDIR}/iptables ${D}${sysconfdir}/network/iptables
}
