# Extend standard hostapd with the rtl871x driver and FoxHub configuration
SRC_URI += " \
    https://raw.githubusercontent.com/pritambaral/hostapd-rtl871xdrv/master/rtlxdrv.patch;name=patch \
    file://hostapd.conf \
"

do_install_prepend() {
    install -m 0644 ${WORKDIR}/hostapd.conf ${S}/${PN}
}

SRC_URI[patch.md5sum] = "ca8697f0254e3444f1e3758100c4f949"
SRC_URI[patch.sha256sum] = "44654c7386503c01d18278ef6d1107da6880c3bba120737c7012e4a8b5c073ce"
