# Base this image on the Raspberry Pi basic image (boots + ssh server)
include recipes-core/images/rpi-basic-image.bb

IMAGE_FEATURES += " package-management"

IMAGE_INSTALL += " \
	util-linux \
	usbutils \
	iptables \
	dnsmasq \
	wpa-supplicant \
	wpa-supplicant-cli \
	wpa-supplicant-passphrase \
	wireless-tools \
	hostapd \
	avahi-daemon \
	nodejs \
	nodejs-npm \
	nodejs-systemtap \
	"

ROOTFS_POSTPROCESS_COMMAND += "foxhub_enable_forwarding; "

foxhub_enable_forwarding() {
	IMAGE_SYSCTL_CONF="${IMAGE_ROOTFS}${sysconfdir}/sysctl.conf"
	echo "net.ipv4.ip_forward = 1" >> ${IMAGE_SYSCTL_CONF}
}
