# FoxHub

This repository contains a collection of metadata for [Yocto](http://yoctoproject.org/) to prototype a wireless router on Raspberry Pi hardware.

## Build from scratch

1.  Check out Yocto from git:

    ```
git clone http://git.yoctoproject.org/cgit/cgit.cgi/poky yocto --branch jethro
```

2.  Switch to the yocto checkout and clone the metadata repositories (including this repository):

    ```
cd yocto
git clone http://cgit.openembedded.org/meta-openembedded --branch jethro
git clone http://git.yoctoproject.org/cgit/cgit.cgi/meta-raspberrypi --branch jethro
git clone https://gitlab.com/Cwiiis/meta-foxhub.git
```

3.  Source the build environment script:

    ```
. oe-init-build-env
```

4.  Configure your build. Assuming you're building for Raspberry Pi 2, add the following two lines to `local.conf` contained in the `conf` directory:

    ```
MACHINE ??= "raspberrypi2"
GPU_MEM = "16"
```

    And add the metadata directories to your `bblayers.conf` file, contained in the `conf` directory, so it reads something like this:

    ```
BBLAYERS ?= " \
  /home/user/yocto/meta \
  /home/user/yocto/meta-yocto \
  /home/user/yocto/meta-yocto-bsp \
  /home/user/yocto/meta-openembedded/meta-oe \
  /home/user/yocto/meta-openembedded/meta-python \
  /home/user/yocto/meta-openembedded/meta-networking \
  /home/user/yocto/meta-raspberrypi \
  /home/user/yocto/meta-foxhub \
  "
```

5.  Build your image:

    ```
bitbake rpi-foxhub
```

6.  Flash the image located in `tmp/deploy/images/raspberrypi2/` to an SD card. Make sure to use the correct device for your SD card:

    ```
sudo dd if=tmp/deploy/images/raspberrypi2/rpi-foxhub-raspberrypi2-TIMESTAMP.rootfs.rpi-sdimg of=/dev/sdb && sync
```

7.  Recommended: Expand the root partition to fill the SD card. This can be done in Linux using the `gparted` tool. (TODO: Do this automatically)

8.  Done! Place SD card into a Raspberry Pi and turn it on. You should now have a forwarding router with ESSID `FoxHub` and password `WhatTheFoxSays!`.

## Rebuilding

Yocto is clever enough that if you change any dependent files of a build, only the necessary parts of the build will be recomputed. If nothing has changed, rebuilding an image will do nothing.

1.  Change to the yocto checkout directory and source the build environment script:

    ```
. oe-init-build-env
```

2.  Resume from step 5 above.

## Updating a specific package / Installing a new package

1.  Use bitbake to build the specific changed package, e.g.:

    ```
bitbake nodejs
```

2.  Copy the package from `tmp/deploy/rpm/cortexa7hf_vfp_vfpv4_neon/` to the device, e.g.:

    ```
scp tmp/deploy/rpm/cortexa7hf_vfp_vfpv4_neon/nodejs-4.2.6-r0.cortexa7hf_vfp_vfpv4_neon.rpm root@192.168.1.1:
```

3.  Use rpm to install the package from a terminal or SSH session on the device, e.g.:
    ```
rpm -U --force nodejs-4.2.6-r0.cortexa7hf_vfp_vfpv4_neon.rpm
```

## Adding new software to the default image

Add the package name to the [image recipe](recipes/images/rpi-foxhub.bb).
